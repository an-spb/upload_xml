import os
from xml.etree.ElementTree import ParseError
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings')
from django.conf import settings

from xml_reader.models import DataFile
from xml_reader.utils import MemcacheClientSingleton
    

app = Celery('tasks', broker=settings.CELERY_CONNECTION)


@app.task
def calculate_file_completeness(file_id):
    mc = MemcacheClientSingleton(settings.MEMCACHED_SERVERS, debug=1)
    memcache_key = 'completeness_progress_%s' % file_id
        
    try:
        df = DataFile.objects.get(id=file_id)
        
        def update_progress(total, processed):
            mc.set(memcache_key, {'progress': {'items_num': total,
                                               'processed_num': processed}})
        
        try:
            total, found = df.get_completeness(progress_callback=update_progress)
        except ParseError:
            mc.set(memcache_key, {'error': True, 'xml_parse_error': True})
            return

        mc.set(memcache_key, {'result': {'itemfields_num': total,
                                         'found_num': found}})
    except Exception:
        mc.set(memcache_key, {'error': True})
        raise

