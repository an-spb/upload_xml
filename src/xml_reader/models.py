import time
from xml.etree import ElementTree
from django.db import models


class DataFile(models.Model):
    file = models.FileField()
    
    ITEM_FIELDS = ('ID', 'IsCollection', 'Name', 'Number_KP', 'IdCollection',
                   'NameCollection', 'Date', 'Datebegin', 'Datend', 'Country',
                   'Place', 'Place_English', 'Org', 'Description', 'MusColl',
                   'MusColl_ID', 'MusImportance', 'Barcode')
    
    def get_completeness(self, progress_callback=lambda total, processed: None):
        '''
        Returns tuple (total, found)
            total -- number of items in file multiplied by number of item required fields
            found -- number of item-field pairs found in file
        Raises xml.etree.ElementTree.ParseError in case of incorrect xml file 
        '''
        
        xml_tree = ElementTree.parse(self.file)
        items = xml_tree.findall('Item')

        N = len(items)
        fields_found = 0

        for i, item in enumerate(items):
            for field in self.ITEM_FIELDS:
                elem = item.find(field)
                if elem is not None:
                    if elem.text and elem.text.strip(' \t\n\r'):
                        fields_found += 1
            
            time.sleep(1)
            progress_callback(N, i + 1)
        
        return N * len(self.ITEM_FIELDS), fields_found
        
