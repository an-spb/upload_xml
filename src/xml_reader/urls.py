from django.conf.urls import patterns, url
from xml_reader.views import UploadView, ProcessXML, GetProcessXMLProgress


urlpatterns = patterns('',
    url(r'^$', UploadView.as_view()),    
    url(r'^process_xml$', ProcessXML.as_view()),    
    url(r'^get_process_xml_status$', GetProcessXMLProgress.as_view()),    
)
