import os
import json
import memcache
from django.views.generic.base import View
from django.shortcuts import render_to_response
from django.http.response import JsonResponse
from django.conf import settings
from xml_reader.models import DataFile
from xml_reader.tasks import calculate_file_completeness
from xml_reader.utils import MemcacheClientSingleton


class UploadView(View):
    def get(self, request, **kwargs):
        return render_to_response('xml_reader/upload.html')


class ProcessXML(View):
    def post(self, request, **kwargs):
        if not 'file' in request.FILES:
            raise Exception('File not found in post request')
                
        df = DataFile.objects.create(file=request.FILES['file'])
        
        calculate_file_completeness.delay(df.id)
        
        response = {'file_id': df.id,
                    'file_name': os.path.basename(df.file.name)}
        
        return JsonResponse(response)


class GetProcessXMLProgress(View):
    def get(self, request, **kwargs):
        if not self.test_memcached_connection():
            return JsonResponse({'error': True})
    
        file_id = request.GET['file_id']

        memcache_key = 'completeness_progress_%s' % file_id
        
        mc = MemcacheClientSingleton(settings.MEMCACHED_SERVERS)
        progress_info = mc.get(memcache_key)
        
        if not progress_info:
            return JsonResponse({'pending': True});        
        elif 'result' in progress_info or 'error' in progress_info:
            mc.delete(memcache_key)

        return JsonResponse(progress_info)

    def test_memcached_connection(self):
        mc = MemcacheClientSingleton(settings.MEMCACHED_SERVERS)
        mc.set('test_key', 'X')
        return mc.get('test_key') == 'X'
        
