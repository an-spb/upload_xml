var UploadView = Backbone.View.extend({
    events: {
        'click #upload-btn': 'run_file_processing'
    },
    
    initialize: function ( attributes, options ) {
        this.lock_ui(false);
    },
    
    run_file_processing: function () {
        var form_data = new FormData(),
            file = this.$('#upload-file');
        
        if (!file.val()) {
            alert('Select file, please');
            return;
        }
        
        form_data.append('file', file[0].files[0]);
        
        this.lock_ui(true);
    
        $.ajax({
            url: "/process_xml",
            type: "POST",
            data: form_data,
            dataType: 'json',
            processData: false,  // tell jQuery not to process the data
            contentType: false   // tell jQuery not to set contentType
        })
        .done(_.bind(this.render_file, this))
        .fail(function () {
            alert('Server error occured =(');
        })
        .always(_.bind(function () {
            file.replaceWith(file.clone());
            this.lock_ui(false);
        }, this));
    },
    
    render_file: function ( file_desc ) {
        var $file = this.$('#file-tmpl').clone().prependTo(this.$('#uploaded-files')),
            $info = $file.find('.info');
        
        $file.show().attr('id', '');
        $file.find('.file-name').html(file_desc.file_name);
        $info.html('pending...')
        
        var MAX_PENDING_ROUNDS = 1000,
        	pending_round_num = 0;
        
        var get_progress_info = _.bind(function () {
            $.get({
                url: "/get_process_xml_status",
                data: {'file_id': file_desc.file_id},
                dataType: 'json'
            })
            .done(_.bind(function ( data ) {
                var info_txt;
                
                if ('progress' in data) {
                    var p = this.percents(data.progress.items_num, data.progress.processed_num);
                    info_txt = 'progress: ' + p;
                } else if ('xml_parse_error' in data) {
                    info_txt = '... is not correct xml file';
                } else if ('error' in data) {
                    info_txt = 'error';
                } else if ('result' in data) {
                    if (data.result.itemfields_num) {
                        var p = this.percents(data.result.itemfields_num, data.result.found_num);
                        info_txt = 'result: data completeness ' + p;
                    } else {
                        info_txt = '0 items in file';
                    }
                } else if (pending_round_num++ > MAX_PENDING_ROUNDS) {
                	return;
                }

                $info.html(info_txt);
                
                if (!('result' in data) && !('error' in data)) {
                    setTimeout(get_progress_info, 500);
                }
            }, this))
            .fail(function () {
                $file.find('.info').html('error');
            });
        }, this);
        
        get_progress_info();
    },
    
    lock_ui: function ( is_blocked ) {
        this.$('input, button').attr('disabled', is_blocked)
        this.$('.loading-img').toggle(is_blocked)
    },
    
    percents: function ( b, a ) {
        return (a / b * 100).toFixed() + '%';
    }
});
