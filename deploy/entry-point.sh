cd /home/docker/code/src
python manage.py syncdb --noinput
python manage.py collectstatic --noinput

supervisord -n -c /home/docker/code/deploy/supervisord.conf

